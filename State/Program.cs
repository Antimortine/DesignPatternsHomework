﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State
{
    class Program
    {
        static void Main(string[] args)
        {
            CopyMachine m1 = new CopyMachine();
            m1.Pay(33);
            m1.ChooseDevice(Device.USBFlash);
            m1.ChooseDocument("tysyachi poganye.doc");
            m1.Print();
            m1.PrintMore(true);
            m1.ChooseDocument("yet another doc");
            m1.Print();
            m1.PrintMore(false);
            m1.GetOddMoney();

            Console.WriteLine("\n\nSome error examples...");
            try
            {
                CopyMachine m2 = new CopyMachine();
                m2.Print();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("\n");
            try
            {
                CopyMachine m2 = new CopyMachine();
                m2.Pay(5);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine();

            Console.WriteLine("\n");
            try
            {
                CopyMachine m2 = new CopyMachine();
                m2.Pay(15);
                m2.ChooseDevice(Device.WiFiDevice);
                m2.ChooseDocument("doc1");
                m2.Print();
                m2.PrintMore(true);
                m2.ChooseDocument("doc2");
                m2.Print();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine();
        }
    }
}
