﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State
{
    public class CopyMachine
    {
        public IState State { get; set; }
        public double InsertedAmount { get; set; }
        public double Price { get { return 10; } } // Печать одного документа стоит 10 у.е.
        public string DocumentName { get; set; }

        public CopyMachine()
        {
            Console.WriteLine($"Printing one document is {Price} conventional units.");
            Console.WriteLine("Put money in the terminal.");
            State = new InitState();
        }

        public void Pay(double money)
        {
            State.Pay(this, money);
        }

        public void ChooseDevice(Device device)
        {
            State.ChooseDevice(this, device);
        }

        public void ChooseDocument(string documentName)
        {
            State.ChooseDocument(this, documentName);
        }

        public void Print()
        {
            State.Print(this);
        }

        public void PrintMore(bool isNeeded)
        {
            State.PrintMore(this, isNeeded);
        }

        public void GetOddMoney()
        {
            State.GetOddMoney(this);
        }
    }

    public enum Device
    {
        USBFlash,
        WiFiDevice
    }

    public interface IState
    {
        void Pay(CopyMachine context, double money);
        void ChooseDevice(CopyMachine context, Device device);
        void ChooseDocument(CopyMachine context, string documentName);
        void Print(CopyMachine context);
        void PrintMore(CopyMachine context, bool isNeeded);
        void GetOddMoney(CopyMachine context);
    }

    public abstract class StateBase : IState
    {
        protected abstract string _msg { get; }
        public virtual void Pay(CopyMachine context, double money)
        {
            throw new Exception(_msg);
        }

        public virtual void ChooseDevice(CopyMachine context, Device device)
        {
            throw new Exception(_msg);
        }

        public virtual void ChooseDocument(CopyMachine context, string documentName)
        {
            throw new Exception(_msg);
        }

        public virtual void Print(CopyMachine context)
        {
            throw new Exception(_msg);
        }

        public virtual void PrintMore(CopyMachine context, bool isNeeded)
        {
            throw new Exception(_msg);
        }

        public virtual void GetOddMoney(CopyMachine context)
        {
            throw new Exception(_msg);
        }
    }

    public class InitState : StateBase
    {
        protected override string _msg => "You have to pay first!";

        public override void Pay(CopyMachine context, double money)
        {
            if (money < context.Price)
                throw new Exception($"The minimum amount of money is {context.Price} conventional units.");        
            context.InsertedAmount = money;
            Console.WriteLine($"Money entered: {money}.");
            Console.WriteLine("Select source device:");
            context.State = new DeviceSelectionState();
        }
    }

    public class DeviceSelectionState : StateBase
    {
        protected override string _msg => "You have to select device!";

        public override void ChooseDevice(CopyMachine context, Device device)
        {
            switch (device)
            {
                case Device.USBFlash:
                    Console.WriteLine("USB device has been chosen.");
                    break;
                case Device.WiFiDevice:
                    Console.WriteLine("Wi-Fi device has been chosen.");
                    break;
            }
            Console.WriteLine("Select document:");
            context.State = new DocumentSelectionState();
        }
    }

    public class DocumentSelectionState : StateBase
    {
        protected override string _msg => "You have to choose the document!";

        public override void ChooseDocument(CopyMachine context, string documentName)
        {
            Console.WriteLine($"The document \"{documentName}\" has been chosen.");
            context.DocumentName = documentName;
            Console.WriteLine("Click the \"print\" button!");
            context.State = new PrintState();
        }
    }

    public class PrintState : StateBase
    {
        protected override string _msg => "You can only print the document at the moment!";

        public override void Print(CopyMachine context)
        {
            context.InsertedAmount -= context.Price;
            if (context.InsertedAmount < 0)
            {
                throw new Exception("Not enough money for the transaction!");
            }

            Console.WriteLine($"The document \"{context.DocumentName}\" is printed.");
            Console.WriteLine("Do you want to print more documents?");
            context.State = new MorePrintState();
        }
    }

    public class MorePrintState : StateBase
    {
        protected override string _msg => "You can only choose \"yes\" or \"no\" at the moment!";

        public override void PrintMore(CopyMachine context, bool isNeeded)
        {
            if(isNeeded)
            {
                Console.WriteLine("You have decided to print more documents.");
                context.State = new DocumentSelectionState();
            }
            else
            {
                Console.WriteLine("Take the odd money.");
                context.State = new TakeOddMoneyState();
            }
        }
    }

    public class TakeOddMoneyState : StateBase
    {
        protected override string _msg => "Shut up and take your money!";

        public override void GetOddMoney(CopyMachine context)
        {
            Console.WriteLine($"You got the change: {context.InsertedAmount}.");
            context.State = new FinishState();
            Console.WriteLine("Thanks!");
        }

    }

    public class FinishState : StateBase
    {
        protected override string _msg => "Error: the work has been finished!";
    }
}
