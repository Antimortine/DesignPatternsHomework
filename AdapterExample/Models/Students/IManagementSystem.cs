﻿using AdapterExample.Models.Database;

namespace AdapterExample.Models.Students
{
    interface IManagementSystem
    {
        DbUserEntity GetStudent(int id);
        DbUserInfoEntity GetStudentInfo(int id);
        void AddStudent(DbUserEntity student);
        void AddStudentInfo(DbUserInfoEntity studentInfo);
        void UpdateStudent(DbUserEntity student);
        void UpdateStudentInfo(DbUserInfoEntity studentInfo);
        void DeleteStudent(int id);
        void DeleteStudentInfo(int id);
    }
}
