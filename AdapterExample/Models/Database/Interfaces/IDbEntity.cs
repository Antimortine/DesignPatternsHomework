﻿namespace AdapterExample.Models.Database.Interfaces
{
    public interface IDbEntity
    {
        int Id { get; set; }
    }
}
