﻿using AdapterExample.Models.Database.Interfaces;

namespace AdapterExample.Models.Database
{
    public class DbUserEntity : IDbEntity
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string PasswordHash { get; set; }
        public int UserInfoId { get; set; }
    }
}
