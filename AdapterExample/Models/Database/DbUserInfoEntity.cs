﻿using AdapterExample.Models.Database.Interfaces;
using System;

namespace AdapterExample.Models.Database
{
    public class DbUserInfoEntity : IDbEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Birthday { get; set; }
    }
}
