﻿using AdapterExample.Models.Database;
using AdapterExample.Models.Students;

namespace AdapterExample.ObjectAdapters
{
    abstract class AbstractAdapter : IManagementSystem
    {
        public abstract void AddStudent(DbUserEntity student);
        public abstract void AddStudentInfo(DbUserInfoEntity studentInfo);
        public abstract void DeleteStudent(int id);
        public abstract void DeleteStudentInfo(int id);
        public abstract DbUserEntity GetStudent(int id);
        public abstract DbUserInfoEntity GetStudentInfo(int id);
        public abstract void UpdateStudent(DbUserEntity student);
        public abstract void UpdateStudentInfo(DbUserInfoEntity studentInfo);
    }
}
