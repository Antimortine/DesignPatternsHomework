﻿using AdapterExample.Models.Database;
using AdapterExample.SecondOrmLibrary;
using System.Linq;

namespace AdapterExample.ObjectAdapters
{
    class SecondOrmAdapter : AbstractAdapter
    {
        private readonly ISecondOrm _secondOrm;

        public SecondOrmAdapter(ISecondOrm secondOrm)
        {
            _secondOrm = secondOrm;
        }

        public override void AddStudent(DbUserEntity student)
        {
            _secondOrm.Context.Users.Add(student);
        }

        public override void AddStudentInfo(DbUserInfoEntity studentInfo)
        {
            _secondOrm.Context.UserInfos.Add(studentInfo);
        }

        public override void DeleteStudent(int id)
        {
            _secondOrm.Context.Users.RemoveWhere(u => u.Id == id);
        }

        public override void DeleteStudentInfo(int id)
        {
            _secondOrm.Context.UserInfos.RemoveWhere(ui => ui.Id == id);
        }

        public override DbUserEntity GetStudent(int id)
        {
            return _secondOrm.Context.Users.Where(u => u.Id == id).First();
        }

        public override DbUserInfoEntity GetStudentInfo(int id)
        {
            return _secondOrm.Context.UserInfos.Where(ui => ui.Id == id).First();
        }

        public override void UpdateStudent(DbUserEntity student)
        {
            DeleteStudent(student.Id);
            _secondOrm.Context.Users.Add(student);
        }

        public override void UpdateStudentInfo(DbUserInfoEntity studentInfo)
        {
            DeleteStudentInfo(studentInfo.Id);
            _secondOrm.Context.UserInfos.Add(studentInfo);
        }
    }
}
