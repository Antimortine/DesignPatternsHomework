﻿using AdapterExample.FirstOrmLibrary;
using AdapterExample.Models.Database;

namespace AdapterExample.ObjectAdapters
{
    class FirstOrmAdapter : AbstractAdapter
    {
        private readonly IFirstOrm<DbUserEntity> _userFirstOrm;
        private readonly IFirstOrm<DbUserInfoEntity> _userInfoFirstOrm;

        public FirstOrmAdapter(IFirstOrm<DbUserEntity> userFirstOrm, IFirstOrm<DbUserInfoEntity> userInfoFirstOrm)
        {
            _userFirstOrm = userFirstOrm;
            _userInfoFirstOrm = userInfoFirstOrm;
        }

        public override void AddStudent(DbUserEntity student)
        {
            _userFirstOrm.Create(student);
        }

        public override void AddStudentInfo(DbUserInfoEntity studentInfo)
        {
            _userInfoFirstOrm.Create(studentInfo);
        }

        public override void DeleteStudent(int id)
        {
            _userFirstOrm.Delete(_userFirstOrm.Read(id));
        }

        public override void DeleteStudentInfo(int id)
        {
            _userInfoFirstOrm.Delete(_userInfoFirstOrm.Read(id));
        }

        public override DbUserEntity GetStudent(int id)
        {
            return _userFirstOrm.Read(id);
        }

        public override DbUserInfoEntity GetStudentInfo(int id)
        {
            return _userInfoFirstOrm.Read(id);
        }

        public override void UpdateStudent(DbUserEntity student)
        {
            _userFirstOrm.Update(student);
        }

        public override void UpdateStudentInfo(DbUserInfoEntity studentInfo)
        {
            _userInfoFirstOrm.Update(studentInfo);
        }
    }
}
