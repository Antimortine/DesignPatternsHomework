﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarsFactory
{
    #region Interfaces

    public interface ICarFactory
    {
        ICarBody CreateBody();
        IEngine CreateEngine();
        IInterior CreateInterior();
        IChassis CreateChassis();
    }

    public interface ICarBody
    {
        string Type { get; }
    }

    public interface IEngine
    {
        string Type { get; }
    }

    public interface IInterior
    {
        string Type { get; }
    }

    public interface IChassis
    {
        string Type { get; }
    }

    #endregion

    #region Porsche Factory

    public class PorscheFactory : ICarFactory
    {
        public ICarBody CreateBody()
        {
            return new PorscheBody();
        }
        public IEngine CreateEngine()
        {
            return new PorscheEngine();
        }
        public IInterior CreateInterior()
        {
            return new PorscheInterior();
        }
        public IChassis CreateChassis()
        {
            return new PorscheChassis();
        }
    }

    public class PorscheBody : ICarBody
    {
        public string Type
        {
          get { return "Porsche body"; }
        }
    }

    public class PorscheEngine : IEngine
    {
        public string Type
        {
            get { return "Porsche engine"; }
        }
    }

    public class PorscheInterior : IInterior
    {
        public string Type
        {
            get { return "Porsche interior"; }
        }
    }

    public class PorscheChassis : IChassis
    {
        public string Type
        {
            get { return "Porsche chassis"; }
        }
    }

    #endregion

    #region Ferrari Factory

    public class FerrariFactory : ICarFactory
    {
        public ICarBody CreateBody()
        {
            return new FerrariBody();
        }
        public IEngine CreateEngine()
        {
            return new FerrariEngine();
        }
        public IInterior CreateInterior()
        {
            return new FerrariInterior();
        }
        public IChassis CreateChassis()
        {
            return new FerrariChassis();
        }
    }

    public class FerrariBody : ICarBody
    {
        public string Type
        {
            get { return "Ferrari body"; }
        }
    }

    public class FerrariEngine : IEngine
    {
        public string Type
        {
            get { return "Ferrari engine"; }
        }
    }

    public class FerrariInterior : IInterior
    {
        public string Type
        {
            get { return "Ferrari interior"; }
        }
    }

    public class FerrariChassis : IChassis
    {
        public string Type
        {
            get { return "Ferrari chassis"; }
        }
    }

    #endregion
    class Program
    {
        static void Main(string[] args)
        {
            var ferrariFactory = new FerrariFactory();
            var ferrariBody = ferrariFactory.CreateBody();
            var ferrariEngine = ferrariFactory.CreateEngine();
            var ferrariInterior = ferrariFactory.CreateInterior();
            var ferrariChassis = ferrariFactory.CreateChassis();
            Console.WriteLine("Тип кузова: {0}", ferrariBody.Type);
            Console.WriteLine("Тип двигателя: {0}", ferrariEngine.Type);
            Console.WriteLine("Тип салона: {0}", ferrariInterior.Type);
            Console.WriteLine("Тип шасси: {0}", ferrariChassis.Type);
        }
    }
}
