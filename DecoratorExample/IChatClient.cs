﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorExample
{
    public interface IChatClient
    {
        Message SendMessage(Message message);
        List<Message> GetMessages(string addressee);
    }
}
