﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorExample
{
    class Program
    {
        static void Main(string[] args)
        {
            IChatClient client = new ChatClient();

            client = new DecoratorBuilder(client)
                .WithMessageEncryption()
                .WithUsernameObfuscation()
                .Build();

            Message msg = new Message("Первый", "Второй", "Пшшшшш");
            Console.WriteLine($"Изначальное сообщение:\n\n{msg}");

            msg = client.SendMessage(msg);
            Console.WriteLine($"Зашифрованное обфусцированное сообщение:\n\n{msg}");

            // Получаем все сообщения, предназначенные для "Второй"
            List<Message> msgs = client.GetMessages("Второй");
            Console.WriteLine($"Расшифрованное деобфусцированное сообщение:\n");
            msgs.ForEach(m => Console.WriteLine(m));
        }
    }
}
