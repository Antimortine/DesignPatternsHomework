﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorExample
{
    public class Decorator : IChatClient
    {
        protected readonly IChatClient Decoratee;

        protected Decorator(IChatClient chatclient)
        {
            Decoratee = chatclient;
        }

        public virtual Message SendMessage(Message message)
        {
            message = OnBeforeSend(message);
            Decoratee.SendMessage(message);
            OnAfterSend(message);
            return message;
        }

        public List<Message> GetMessages(string addressee)
        {
            addressee = OnBeforeGet(addressee);
            var result = Decoratee.GetMessages(addressee);
            return OnAfterGet(result);
        }

        protected virtual Message OnBeforeSend(Message message)
        {
            return message;
        }

        protected virtual void OnAfterSend(Message message)
        {
            return;
        }

        protected virtual string OnBeforeGet(string result)
        {
            return result;
        }

        protected virtual List<Message> OnAfterGet(List<Message> result)
        {
            return result;
        }
    }

    public class ObfuscatorDecorator : Decorator
    {
        public ObfuscatorDecorator(IChatClient chatclient) : base(chatclient)
        { }

        private string Obfuscate(string s)
        {
            return $"<obfuscated>{s}</obfuscated>";
        }

        private string Deobfuscate(string s)
        {
            return s.Substring(12, s.Length - 25);
        }

        protected override Message OnBeforeSend(Message message)
        {
            message.Addressee = Obfuscate(message.Addressee);
            message.Author = Obfuscate(message.Author);
            return base.OnBeforeSend(message);
        }

        protected override string OnBeforeGet(string result)
        {
            return Obfuscate(result);
        }

        protected override List<Message> OnAfterGet(List<Message> result)
        {
            result.ForEach(m => { m.Addressee = Deobfuscate(m.Addressee); m.Author = Deobfuscate(m.Author);});
            return result;
        }
    }

    public class CryptoDecorator : Decorator
    {
        public CryptoDecorator(IChatClient chatclient) : base(chatclient)
        { }

        private string Encrypt(string s)
        {
            return $"<encrypted>{s}</encrypted>";
        }

        private string Decrypt(string s)
        {
            return s.Substring(11, s.Length - 23);
        }

        protected override Message OnBeforeSend(Message message)
        {
            message.Text = Encrypt(message.Text);
            return base.OnBeforeSend(message);
        }

        protected override List<Message> OnAfterGet(List<Message> result)
        {
            result.ForEach(m => m.Text = Decrypt(m.Text));
            return result;
        }
    }
}
