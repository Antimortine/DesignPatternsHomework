﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorExample
{
    public class DecoratorBuilder
    {
        private IChatClient ChatClient;

        public DecoratorBuilder(IChatClient chatclient)
        {
            ChatClient = chatclient;
        }

        public DecoratorBuilder WithUsernameObfuscation()
        {
            ChatClient = new ObfuscatorDecorator(ChatClient);
            return this;
        }

        public DecoratorBuilder WithMessageEncryption()
        {
            ChatClient = new CryptoDecorator(ChatClient);
            return this;
        }

        public IChatClient Build()
        {
            return ChatClient;
        }
    }
}
