﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorExample
{
    public class Message
    {
        public string Author;
        public string Addressee;
        public string Text;

        public Message(string author, string addressee, string text)
        {
            Author = author;
            Addressee = addressee;
            Text = text;
        }

        public override string ToString()
        {
            return $"Сообщение от {Author} для {Addressee}:\n{Text}\n\n";
        }
    }
}
