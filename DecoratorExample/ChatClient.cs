﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorExample
{
    public class ChatClient : IChatClient
    {
        private Dictionary<string, List<Message>> messages = new Dictionary<string, List<Message>>();
        // Ключ - адресат (addressee)

        public List<Message> GetMessages(string addressee)
        {
            return messages[addressee];
        }

        public Message SendMessage(Message message)
        {
            if (!messages.ContainsKey(message.Addressee))
                messages[message.Addressee] = new List<Message>();
            messages[message.Addressee].Add(message);
            return message;
        }
    }
}
