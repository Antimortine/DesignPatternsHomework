﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    class Program
    {
        static Bancomat BANKOMAT = new Bancomat();
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            // Для корректного отображения символов валют следует в настройках консоли
            // выбрать любой TrueType шрифт, например, Consolas

            //TestValidate();
            TestGetMoney(CurrencyType.Ruble, 7670);
            TestGetMoney(CurrencyType.Ruble, 7673);
            TestGetMoney(CurrencyType.Dollar, 660);
            TestGetMoney(CurrencyType.Dollar, 666);
            TestGetMoney(CurrencyType.Eur, 500); // В банкомате нет евро

        }

        static void TestGetMoney(CurrencyType cType, int amount)
        {
            Console.WriteLine($"Пытаемся разменять сумму {new Banknote(cType, amount)}...");
            try
            {
                BANKOMAT.GetMoney(cType, amount);
            }
            catch (BancomatException be)
            {
                Console.WriteLine(be.Message);
            }
            Console.WriteLine();
        }

        static void TestValidate()
        {
            Validate(new Banknote(CurrencyType.Ruble, 10));
            Validate(new Banknote(CurrencyType.Ruble, 50));
            Validate(new Banknote(CurrencyType.Ruble, 100));
            Validate(new Banknote(CurrencyType.Ruble, 500));
            Validate(new Banknote(CurrencyType.Ruble, 1000));
            Validate(new Banknote(CurrencyType.Ruble, 5000));
            Validate(new Banknote(CurrencyType.Ruble, 11));
            Validate(new Banknote(CurrencyType.Dollar, 10));
            Validate(new Banknote(CurrencyType.Dollar, 50));
            Validate(new Banknote(CurrencyType.Dollar, 100));
            Validate(new Banknote(CurrencyType.Dollar, 22));
        }

        static void Validate(Banknote banknote)
        {
            Console.Write($"Проверяем банкноту {banknote}. ");
            Console.WriteLine($"Результат проверки: {BANKOMAT.Validate(banknote)}");
        }
    }
}
