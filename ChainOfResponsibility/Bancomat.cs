﻿using System;
using System.Collections.Generic;

namespace ChainOfResponsibility
{
    public enum CurrencyType
    {
        Eur,
        Dollar,
        Ruble
    }

    public class Banknote
    {
        public CurrencyType Currency { get; }
        public int Value { get; }

        protected Dictionary<CurrencyType, string> CurrencyMap = new Dictionary<CurrencyType, string>
        {
            {CurrencyType.Dollar, "$"},
            {CurrencyType.Eur, "€"},
            {CurrencyType.Ruble, "₽"}
        };
        public string Sign => CurrencyMap[Currency];

        public Banknote(CurrencyType currency, int value)
        {
            Currency = currency;
            Value = value;
        }
        public override string ToString()
        {
            return $"{Value}{Sign}";
        }
    }

    public class BancomatException : Exception
    {
        public BancomatException(string message) : base(message) { }
    }

    public class Bancomat
    {
        private readonly BanknoteHandler _handler;

        public Bancomat()
        {
            _handler = new TenRubleHandler(null);
            _handler = new FiftyRubleHandler(_handler);
            _handler = new HundredRubleHandler(_handler);
            _handler = new FiveHundredRubleHandler(_handler);
            _handler = new ThousandRubleHandler(_handler);
            _handler = new FiveThousandRubleHandler(_handler);
            _handler = new TenDollarHandler(_handler);
            _handler = new FiftyDollarHandler(_handler);
            _handler = new HundredDollarHandler(_handler);
        }

        public bool Validate(Banknote banknote)
        {
            return _handler.Validate(banknote);
        }

        public void GetMoney(Banknote banknote)
        {
            _handler.GetMoney(banknote);
        }

        public void GetMoney(CurrencyType currencyType, int value)
        {
            GetMoney(new Banknote(currencyType, value));
        }
    }

    public abstract class BanknoteHandler
    {
        private readonly BanknoteHandler _nextHandler;

        protected abstract int Value { get; }

        protected abstract CurrencyType Currency { get; }

        protected BanknoteHandler(BanknoteHandler nextHandler)
        {
            _nextHandler = nextHandler;
        }

        public virtual bool Validate(Banknote banknote)
        {
            if (banknote.Currency == Currency && banknote.Value == Value)
            {
                return true;
            }
            return _nextHandler != null && _nextHandler.Validate(banknote);
        }

        public virtual void GetMoney(Banknote banknote)
        {
            if (banknote.Currency != Currency)
            {
                if (_nextHandler != null)
                    _nextHandler.GetMoney(banknote);
                else
                    throw new BancomatException($"Банкомат не может выдать сумму {banknote}");
                return;
            }
            int residue = banknote.Value;
            int n = residue / Value;
            residue = residue - n * Value;
            if (residue != 0)
            {
                if (_nextHandler != null)
                    _nextHandler.GetMoney(new Banknote(banknote.Currency, residue));
                else
                    throw new BancomatException($"Банкомат не может выдать сумму {new Banknote(banknote.Currency, residue)}");
            }
            Console.WriteLine($"Выданы банкноты номиналом {Value}{banknote.Sign} в количестве {n} шт.");
        }
    }

    public abstract class RubleHandlerBase : BanknoteHandler
    {
        protected override CurrencyType Currency => CurrencyType.Ruble;

        protected RubleHandlerBase(BanknoteHandler nextHandler) : base(nextHandler) { }
    }

    public class TenRubleHandler : RubleHandlerBase
    {
        protected override int Value => 10;

        public TenRubleHandler(BanknoteHandler nextHandler) : base(nextHandler) { }
    }

    public class FiftyRubleHandler : RubleHandlerBase
    {
        protected override int Value => 50;

        public FiftyRubleHandler(BanknoteHandler nextHandler) : base(nextHandler) { }
    }

    public class HundredRubleHandler : RubleHandlerBase
    {
        protected override int Value => 100;

        public HundredRubleHandler(BanknoteHandler nextHandler) : base(nextHandler) { }
    }

    public class FiveHundredRubleHandler : RubleHandlerBase
    {
        protected override int Value => 500;

        public FiveHundredRubleHandler(BanknoteHandler nextHandler) : base(nextHandler) { }
    }

    public class ThousandRubleHandler : RubleHandlerBase
    {
        protected override int Value => 1000;

        public ThousandRubleHandler(BanknoteHandler nextHandler) : base(nextHandler) { }
    }

    public class FiveThousandRubleHandler : RubleHandlerBase
    {
        protected override int Value => 5000;

        public FiveThousandRubleHandler(BanknoteHandler nextHandler) : base(nextHandler) { }
    }

    public abstract class DollarHandlerBase : BanknoteHandler
    {
        protected override CurrencyType Currency => CurrencyType.Dollar;

        protected DollarHandlerBase(BanknoteHandler nextHandler) : base(nextHandler) { }
    }

    public class HundredDollarHandler : DollarHandlerBase
    {
        protected override int Value => 100;

        public HundredDollarHandler(BanknoteHandler nextHandler) : base(nextHandler) { }
    }

    public class FiftyDollarHandler : DollarHandlerBase
    {
        protected override int Value => 50;

        public FiftyDollarHandler(BanknoteHandler nextHandler) : base(nextHandler) { }
    }

    public class TenDollarHandler : DollarHandlerBase
    {
        protected override int Value => 10;

        public TenDollarHandler(BanknoteHandler nextHandler) : base(nextHandler) { }
    }
}
