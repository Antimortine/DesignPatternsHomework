﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmailBuilder
{
    class StateEmailBuilder
    {
        public interface IFinalEmailBuilder
        {
            IFinalEmailBuilder AppendBody(string text);
            IFinalEmailBuilder AddRecipient(string recipient);
            string Build();
        }

        public interface IBodyEmailBuilder
        {
            IFinalEmailBuilder AddBody(string text);
        }

        public interface ISubjectEmailBuilder
        {
            IBodyEmailBuilder AddSubject(string subject);
            IFinalEmailBuilder AddBody(string body);
        }
        
        public ISubjectEmailBuilder AddRecipient(string recipient)
        {
            return new SubjectEmailBuilder(recipient);
        }

        private class SubjectEmailBuilder : ISubjectEmailBuilder
        {
            private List<string> _recipients = new List<string>();

            public SubjectEmailBuilder(string recipient)
            {
                _recipients.Add(recipient);
            }

            public IBodyEmailBuilder AddSubject(string subject)
            {
                return new BodyEmailBuilder(_recipients, subject);
            }

            public IFinalEmailBuilder AddBody(string body)
            {
                return new FinalEmailBuilder(_recipients, body);
            }
        }

        private class BodyEmailBuilder : IBodyEmailBuilder
        {
            private List<string> _recipients;
            private string _subject;

            public BodyEmailBuilder(List<string> recipients, string subject)
            {
                _recipients = recipients;
                _subject = subject;
            }
            public IFinalEmailBuilder AddBody(string body)
            {
                return new FinalEmailBuilder(_recipients, body, _subject);
            }
        }

        private class FinalEmailBuilder : IFinalEmailBuilder
        {
            private List<string> _recipients;
            private string _subject;
            private string _body;

            public FinalEmailBuilder(List<string> recipients, string body, string subject = null)
            {
                _recipients = recipients;
                _subject = subject;
                _body = body;
            }

            public IFinalEmailBuilder AppendBody(string text)
            {
                _body += text;
                return this;
            }
            public IFinalEmailBuilder AddRecipient(string recipient)
            {
                _recipients.Add(recipient);
                return this;
            }

            public string Build()
            {
                StringBuilder result = new StringBuilder();
                result.AppendLine(String.IsNullOrEmpty(_subject) ? "" : String.Format("Тема письма: {0}", _subject));
                result.AppendLine(String.Format("Получатель: {0}", _recipients[0]));
                if (_recipients.Count > 1)
                    result.AppendLine(String.Format("Получатели копии: {0}", string.Join(", ", _recipients.Skip(1))));
                result.AppendLine("\n========================================");
                result.AppendLine(_body);
                result.AppendLine("\n========================================");
                return result.ToString();
            }
        }
    }
}
