﻿using System;

namespace EmailBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            //var email = new FluentEmailBuilder("Спортлото", "Включите воду!")
            //    .AddSubject("Горячая вода")
            //    .AddRecipient("Путин")
            //    .AddRecipient("Медведев")
            //    .AppendBody("\nВоду включите!")
            //    .Build();
            //Console.WriteLine(email);

            var email = new StateEmailBuilder()
                    .AddRecipient("Спортлото")
                    .AddSubject("Горячая вода")
                    .AddBody("Включите воду!")
                    .AddRecipient("Путин")
                    .AppendBody("\nВоду включите!")
                    .AddRecipient("Медведев")
                    .Build();
            Console.WriteLine(email);
        }
    }
}
