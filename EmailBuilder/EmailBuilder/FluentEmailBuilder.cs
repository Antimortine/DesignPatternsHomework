﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmailBuilder
{
    class FluentEmailBuilder
    {
        private List<string> _recipients = new List<string>();
        private string _body;
        private string _subject;

        public FluentEmailBuilder(string recipient, string body)
        {
            if (String.IsNullOrEmpty(recipient) || String.IsNullOrEmpty(body))
                throw new ArgumentException();
            _recipients.Add(recipient);
            _body = body;
        }
        public FluentEmailBuilder AppendBody(string text)
        {
            _body += text;
            return this;
        }

        public FluentEmailBuilder AddRecipient(string recipient)
        {
            _recipients.Add(recipient);
            return this;
        }

        public FluentEmailBuilder AddSubject(string subject)
        {
            if (!String.IsNullOrEmpty(_subject))
                throw new ArgumentException();
            _subject = subject;
            return this;
        }

        public string Build()
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine( String.IsNullOrEmpty(_subject) ? "" : String.Format("Тема письма: {0}", _subject) );
            result.AppendLine( String.Format("Получатель: {0}", _recipients[0]) );
            if (_recipients.Count > 1)
                result.AppendLine( String.Format("Получатели копии: {0}", string.Join(", ", _recipients.Skip(1))) );
            result.AppendLine("\n========================================");
            result.AppendLine(_body);
            result.AppendLine("\n========================================");
            return result.ToString();
        }
    }
}
