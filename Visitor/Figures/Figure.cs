﻿namespace Visitor
{
    public abstract class Figure
    {
        public abstract void Accept(IVisitor visitor);
    }
}
