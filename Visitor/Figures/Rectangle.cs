﻿namespace Visitor
{
    public class Rectangle : Figure
    {
        public double A { get; }
        public double B { get; }

        public Rectangle(double a, double b)
        {
            A = a;
            B = b;
        }
        public override void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}