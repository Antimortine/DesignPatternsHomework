﻿using System;

namespace Visitor
{
    class GetPerimeterVisitor : IVisitor
    {
        public string OperationName => "GetArea";

        public void Visit(Circle circle)
        {
            Console.WriteLine($"The perimeter of the circle is {2 * Math.PI * circle.Radius}.");
        }

        public void Visit(Triangle triangle)
        {
            double P = (triangle.A + triangle.B + triangle.C);
            Console.WriteLine($"The perimeter of the rectangle is {P}.");
        }

        public void Visit(Rectangle rectangle)
        {
            double P = 2 * (rectangle.A + rectangle.B);
            Console.WriteLine($"The perimeter of the rectangle is {P}.");
        }
    }
}
