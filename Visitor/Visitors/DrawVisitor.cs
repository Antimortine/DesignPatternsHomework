﻿using System;

namespace Visitor
{
    public class DrawVisitor : IVisitor
    {
        public string OperationName => "Draw";
        private int X, Y;

        public DrawVisitor(int x, int y)
        {
            X = x;
            Y = y;
        }

        public void Visit(Circle circle)
        {
            Console.WriteLine($"Drawing the circle at the point ({X}, {Y}).");
        }

        public void Visit(Triangle triangle)
        {
            Console.WriteLine($"Drawing the triangle at the point ({X}, {Y}).");
        }

        public void Visit(Rectangle rectangle)
        {
            Console.WriteLine($"Drawing the rectangle at the point ({X}, {Y}).");
        }
    }
}
