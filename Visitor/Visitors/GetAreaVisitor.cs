﻿using System;

namespace Visitor
{
    class GetAreaVisitor : IVisitor
    {
        public string OperationName => "GetArea";

        public void Visit(Circle circle)
        {
            Console.WriteLine($"The area of the circle is {Math.PI * circle.Radius * circle.Radius}.");
        }

        public void Visit(Triangle triangle)
        {
            double p = (triangle.A + triangle.B + triangle.C) / 2.0;
            double s = Math.Sqrt(p * (p - triangle.A) * (p - triangle.B) * (p - triangle.C));
            Console.WriteLine($"The area of the rectangle is {s}.");
        }

        public void Visit(Rectangle rectangle)
        {
            Console.WriteLine($"The area of the rectangle is {rectangle.A * rectangle.B}.");
        }
    }
}
