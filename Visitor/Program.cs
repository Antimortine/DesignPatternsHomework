﻿using System;

namespace Visitor
{
    class Program
    {
        static void Main(string[] args)
        {
            Figure[] figures = {
                new Circle(1),
                new Rectangle(2, 2),
                new Triangle(3, 4, 5)
            };

            DrawVisitor drawVisitor = new DrawVisitor(10, 10);
            GetAreaVisitor getAreaVisitor = new GetAreaVisitor();
            GetPerimeterVisitor getPerimeterVisitor = new GetPerimeterVisitor();
            foreach (var figure in figures)
            {
                figure.Accept(drawVisitor);
                figure.Accept(getAreaVisitor);
                figure.Accept(getPerimeterVisitor);
                Console.WriteLine();
            }
        }
    }
}
